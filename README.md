# Instructions

## First clone this repository.
```bash
clone https://gitlab.com/ChunFei96/frontend_test.git
```

## Install dependencies. Make sure you already have [`nodejs`](https://nodejs.org/en/) & [`npm`](https://www.npmjs.com/) installed in your system.
```bash
npm install
```

## Run it
```bash
npm start
```

## Imrpovement
```bash
1. UI could be further improved to meet the requirements such as auto-resize tiles, polishing etc. 
2. Code Refactor should be performed 
```
## Note
```bash
I have completed this react weather app by picking up the React and Hook within a week starting from 0 knowledge. 
The app definitely have many rooms to improve especially implementing the proper code structure and design pattern. 
Appreciate the learning opportunity.
```
